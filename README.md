# Clean Greetings

## Context

This is an sample projekt aiming to implement the ideas on **Clean Architecture** as described by Robert C. Martin in this
[blog post](https://blog.cleancoder.com/uncle-bob/2012/08/13/the-clean-architecture.html).

The "domain model" is kept as simple as possible on purpose, as I wanted to concentrate on the clean
architecture concepts themselves. 

The project is built as a multi module project with maven and implemented
in Java. 

## Overview

![Modules Overview](./modules.png)

Modules:

* application: Contains the domain model of this application.
Is not allowed to contains any framework specific code. Depends on no other modules,
all framework modules depend on it. Provides ports (interfaces) for adapters to connect to.
* adapters: framework specific implementations of the incoming and outgoing ports.
* contexts: The application contexts, which represent a runnable artefact and were the used adapters are configured.
