package com.gitlab.thofis.cleangreetings;

import com.gitlab.thofis.cleangreetings.model.Greeting;
import com.gitlab.thofis.cleangreetings.model.Salutation;
import com.gitlab.thofis.cleangreetings.usecases.CreateGreetingUsecase;
import com.gitlab.thofis.cleangreetings.usecases.LoadGreetingsUsecase;
import java.util.Collection;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller

public class GreetingWebController {

  private final LoadGreetingsUsecase loadGreetingsUsecase;
  private final CreateGreetingUsecase createGreetingUsecase;

  public GreetingWebController(LoadGreetingsUsecase loadGreetingsUsecase, CreateGreetingUsecase createGreetingUsecase) {
    this.loadGreetingsUsecase = loadGreetingsUsecase;
    this.createGreetingUsecase = createGreetingUsecase;
  }

  @GetMapping("/greeting2")
  public String greeting(
      @RequestParam(name = "name", required = false, defaultValue = "World") String name,
      @RequestParam(name = "salutation", required = false, defaultValue = "Hello") String salutation,
      Model model) {
    Salutation salutationEnum = salutation.equals(Salutation.BONJOUR.getText()) ? Salutation.BONJOUR : Salutation.HELLO;

    final Collection<? extends Greeting> greetings = loadGreetingsUsecase.execute();

    final boolean greetingExistsAlready = greetings
        .stream()
        .anyMatch(gr -> gr.equals(new Greeting(salutationEnum, name)));

    if (!greetingExistsAlready) {
      createGreetingUsecase.execute(salutationEnum, name);
    }

    model.addAttribute("greetings", greetings);
    return "greeting";
  }

}
