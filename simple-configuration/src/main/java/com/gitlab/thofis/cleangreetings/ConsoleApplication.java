package com.gitlab.thofis.cleangreetings;

import com.gitlab.thofis.cleangreetings.ports.out.GreetingRepository;
import com.gitlab.thofis.cleangreetings.repository.InMemoryGreetingRepository;
import com.gitlab.thofis.cleangreetings.usecases.CreateGreetingUsecase;
import com.gitlab.thofis.cleangreetings.usecases.LoadGreetingsUsecase;

/**
 * Wiring up a simple application configuration using
 * console-ui, inmemory-persistence together with the core application logic.
 */
public class ConsoleApplication {

  private final ConsoleUI consoleUI;

  public ConsoleApplication() {
    GreetingRepository greetingRepository = new InMemoryGreetingRepository();
    CreateGreetingUsecase createGreeting = new CreateGreetingUsecase(greetingRepository);
    LoadGreetingsUsecase loadGreetings = new LoadGreetingsUsecase(greetingRepository);
    consoleUI = new ConsoleUI(createGreeting, loadGreetings);
  }

  public static void main(String[] args) {
    new ConsoleApplication().run();
  }

  public void run() {
    consoleUI.startUILoop();
  }
}
