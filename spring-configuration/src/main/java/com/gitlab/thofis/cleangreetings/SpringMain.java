package com.gitlab.thofis.cleangreetings;

import com.gitlab.thofis.cleangreetings.ports.out.GreetingRepository;
import com.gitlab.thofis.cleangreetings.usecases.CreateGreetingUsecase;
import com.gitlab.thofis.cleangreetings.usecases.LoadGreetingsUsecase;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class SpringMain {

  public static void main(String[] args) {
    SpringApplication.run(SpringMain.class, args);
  }

  @Bean
  public LoadGreetingsUsecase initLoadGreetings(GreetingRepository greetingRepository) {
    return new LoadGreetingsUsecase(greetingRepository);
  }

  @Bean
  public CreateGreetingUsecase initCreateGreeting(GreetingRepository greetingRepository) {
    return new CreateGreetingUsecase(greetingRepository);
  }


}
