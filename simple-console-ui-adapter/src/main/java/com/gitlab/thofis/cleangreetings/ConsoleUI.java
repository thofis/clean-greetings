package com.gitlab.thofis.cleangreetings;

import com.gitlab.thofis.cleangreetings.model.Greeting;
import com.gitlab.thofis.cleangreetings.model.Salutation;
import com.gitlab.thofis.cleangreetings.ports.in.CreateGreeting;
import com.gitlab.thofis.cleangreetings.ports.in.LoadGreetings;
import java.util.Scanner;

public class ConsoleUI {

  private final CreateGreeting createGreeting;
  private final LoadGreetings loadGreetings;
  private final Scanner keyboardScanner;

  public ConsoleUI(CreateGreeting createGreeting, LoadGreetings loadGreetings) {
    this.createGreeting = createGreeting;
    this.loadGreetings = loadGreetings;
    this.keyboardScanner = new Scanner(System.in);
  }

  public void startUILoop() {
    while (true) {
      printChoices();
      processChoice(readOneCharacter());
    }
  }

  private void printChoices() {
    System.out.println("What do want to do:");
    System.out.println("(1) for creating a greeting");
    System.out.println("(2) for listing all existing greetings");
    System.out.println("(3) for quit");
  }

  private char readOneCharacter() {
    final char enteredChar = keyboardScanner.next(".")
        .charAt(0);
    return enteredChar;
  }

  private void processChoice(char choice) {

    switch (choice) {
      case '1':
        final Greeting greeting = createGreeting.execute(processSalutation(), processName());
        System.out.println("Greeting is: " + greeting.greet());
        break;
      case '2':
        var greetings = loadGreetings.execute();
        if (!greetings.isEmpty()) {
          System.out.println("---");
          System.out.println("Currently stored greetings:");
          greetings.forEach(greeting1 -> System.out.println(greeting1));
          System.out.println("---");
        }
        break;
      case '3':
        System.exit(0);
        break;
      default:
        System.out.println("Unknown option: Please try again.");
    }
    System.out.println();
  }

  private Salutation processSalutation() {
    System.out.println("Choose a salutation: 1 for Hello, 2 for Bonjour:");
    return readOneCharacter() == '2' ? Salutation.BONJOUR : Salutation.HELLO;
  }

  private String processName() {
    System.out.println("Now Enter a name please: ");
    return keyboardScanner.next();
  }


}
