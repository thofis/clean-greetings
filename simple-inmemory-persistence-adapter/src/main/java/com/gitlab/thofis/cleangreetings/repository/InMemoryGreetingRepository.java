package com.gitlab.thofis.cleangreetings.repository;

import com.gitlab.thofis.cleangreetings.model.Greeting;
import com.gitlab.thofis.cleangreetings.ports.out.GreetingRepository;
import java.util.Collection;
import java.util.LinkedHashSet;

import static java.util.Collections.unmodifiableCollection;

public class InMemoryGreetingRepository implements GreetingRepository {

  private final Collection<Greeting> greetings;

  public InMemoryGreetingRepository() {
    this.greetings = new LinkedHashSet<>();
  }

  @Override
  public Greeting persistGreeting(Greeting greeting) {
    greetings.add(greeting);
    return greeting;
  }

  @Override
  public Collection<Greeting> getGreetings() {
    return unmodifiableCollection(greetings);
  }
}
