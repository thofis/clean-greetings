package com.gitlab.thofis.cleangreetings.model;

import java.util.stream.Stream;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;
import org.junit.jupiter.params.provider.ValueSource;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class NameTest {
  private static Stream<String> blankStrings() {
    return Stream.of("12", "012345678901234567890", null);
  }

  @ParameterizedTest
  @ValueSource(strings = {
      "Thomas",
      "World",
      "123",
      "01234567890123456789"
  })
  public void createNameSucceeds(String value) {
    assertDoesNotThrow(() -> new Name(value));
  }

  @ParameterizedTest
  @MethodSource("blankStrings")
  public void createNameFails(String value) {
    assertThrows(RuntimeException.class, () -> new Name(value));
  }
}
