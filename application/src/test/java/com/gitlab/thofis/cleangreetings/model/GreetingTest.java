package com.gitlab.thofis.cleangreetings.model;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class GreetingTest {
  @Test
  public void helloWorldWorks() {
    Greeting greeting = new Greeting(Salutation.HELLO, "World");
    Assertions.assertEquals("Hello World!", greeting.greet());
  }
}
