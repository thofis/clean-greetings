package com.gitlab.thofis.cleangreetings;

import com.gitlab.thofis.cleangreetings.model.Greeting;
import com.gitlab.thofis.cleangreetings.ports.in.CreateGreeting;
import com.gitlab.thofis.cleangreetings.ports.out.GreetingRepository;
import com.gitlab.thofis.cleangreetings.usecases.CreateGreetingUsecase;
import java.util.Collection;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static com.gitlab.thofis.cleangreetings.model.Salutation.HELLO;
import static org.junit.jupiter.api.Assertions.assertEquals;

/**
 * Just for testing purposes.
 */
public class CreateGreetingTest {

  @Test
  public void createGreeting() {
    GreetingRepository greetingRepository = new TestGreetingRepository();
    CreateGreeting createGreeting = new CreateGreetingUsecase(greetingRepository);

    final Greeting greeting = createGreeting.execute(HELLO, "World");
    assertEquals("Hello World!", greeting.greet());
  }

  private static class TestGreetingRepository implements GreetingRepository {
    Logger log = LoggerFactory.getLogger(TestGreetingRepository.class);

    @Override
    public Greeting persistGreeting(Greeting greeting) {
      log.debug("Persisting of greeting: {} requested.", greeting);
      return greeting;
    }

    @Override
    public Collection<Greeting> getGreetings() {
      return null;
    }
  }
}
