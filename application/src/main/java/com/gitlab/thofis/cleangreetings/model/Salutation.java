package com.gitlab.thofis.cleangreetings.model;

public enum Salutation {
  HELLO("Hello"),
  BONJOUR("Bonjour");

  private final String text;

  Salutation(String text) {
    this.text = text;
  }

  public String getText() {
    return text;
  }
}
