package com.gitlab.thofis.cleangreetings.ports.in;

import com.gitlab.thofis.cleangreetings.model.Greeting;
import java.util.Collection;

public interface LoadGreetings {
  Collection<? extends Greeting> execute();
}
