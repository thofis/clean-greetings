package com.gitlab.thofis.cleangreetings.model;

import java.util.Objects;

public class Name {
  private final String value;

  public Name(String value) {
    validate(value);
    this.value = value;
  }

  public String getValue() {
    return value;
  }

  private void validate(String value) {
    Objects.requireNonNull(value);
    if (value.length() < 3 || value.length() > 20) {
      throw new IllegalArgumentException("Length of name has to between 3 and 20 characters.");
    }
  }

  @Override
  public String toString() {
    return "Name{" +
        "value='" + value + '\'' +
        '}';
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    Name name = (Name) o;
    return Objects.equals(value, name.value);
  }

  @Override
  public int hashCode() {
    return Objects.hash(value);
  }
}
