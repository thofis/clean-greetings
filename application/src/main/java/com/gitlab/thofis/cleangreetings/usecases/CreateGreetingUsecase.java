package com.gitlab.thofis.cleangreetings.usecases;

import com.gitlab.thofis.cleangreetings.model.Greeting;
import com.gitlab.thofis.cleangreetings.model.Salutation;
import com.gitlab.thofis.cleangreetings.ports.in.CreateGreeting;
import com.gitlab.thofis.cleangreetings.ports.out.GreetingRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class CreateGreetingUsecase implements CreateGreeting {

  private static final Logger log = LoggerFactory.getLogger(CreateGreetingUsecase.class);

  private final GreetingRepository greetingRepository;

  public CreateGreetingUsecase(GreetingRepository greetingRepository) {
    this.greetingRepository = greetingRepository;
  }

  @Override
  public Greeting execute(Salutation salutation, String name) {
    log.debug("create greeting for salutation {} and name {}", salutation, name);
    Greeting greeting = new Greeting(salutation, name);
    return greetingRepository.persistGreeting(greeting);
  }
}
