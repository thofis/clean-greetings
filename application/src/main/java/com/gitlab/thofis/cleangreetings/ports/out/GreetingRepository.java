package com.gitlab.thofis.cleangreetings.ports.out;

import com.gitlab.thofis.cleangreetings.model.Greeting;

import java.util.Collection;

import static java.util.Collections.unmodifiableCollection;

/**
 * Interface to be implemented by persistence adapters.
 * Application specifies its persistence requirements here.
 * Dependency Inversion Principle.
 */
public interface GreetingRepository {
  /**
   * Persist a greeting.
   * @param greeting greeting to persist
   * @return the persisted greeting
   */
  public Greeting persistGreeting(Greeting greeting);

  /**
   * Returns a collection of all persisted greetings.
   * @return a collection.
   */
  public Collection<? extends Greeting> getGreetings();
}
