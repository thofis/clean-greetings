package com.gitlab.thofis.cleangreetings.ports.in;

import com.gitlab.thofis.cleangreetings.model.Greeting;
import com.gitlab.thofis.cleangreetings.model.Salutation;

/**
 * Interface to be used by ui-adapters and implemented within application itself.
 */
public interface CreateGreeting {
  /**
   * Creates a new greeting and persists it.
   *
   * @param salutation
   * @param name
   * @return a new Greeting
   */
  Greeting execute(Salutation salutation, String name);
}
