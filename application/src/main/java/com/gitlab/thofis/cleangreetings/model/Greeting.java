package com.gitlab.thofis.cleangreetings.model;

import java.util.Objects;

/**
 * A greeting consists of a salutation followed by a name.
 */
public class Greeting {
  protected Salutation salutation;
  protected Name name;

  public Greeting(Salutation salutation, String name) {
    this(salutation, new Name(name));
  }

  public Greeting(Salutation salutation, Name name) {
    this.salutation = salutation;
    this.name = name;
  }

  public String greet() {
    return salutation.getText() + " " + name.getValue() + "!";
  }

  @Override
  public String toString() {
    return "Greeting{" +
        "salutation=" + salutation +
        ", name=" + name +
        '}';
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass().isAssignableFrom(o.getClass())) {
      return false;
    }
    Greeting greeting = (Greeting) o;
    return salutation == greeting.salutation &&
        Objects.equals(name, greeting.name);
  }

  @Override
  public int hashCode() {
    return Objects.hash(salutation, name);
  }

  public Salutation getSalutation() {
    return salutation;
  }

  public Name getName() {
    return name;
  }
}
