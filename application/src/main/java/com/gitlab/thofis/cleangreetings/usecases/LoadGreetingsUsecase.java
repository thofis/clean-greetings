package com.gitlab.thofis.cleangreetings.usecases;

import com.gitlab.thofis.cleangreetings.model.Greeting;
import com.gitlab.thofis.cleangreetings.ports.in.LoadGreetings;
import com.gitlab.thofis.cleangreetings.ports.out.GreetingRepository;
import java.util.Collection;

public class LoadGreetingsUsecase implements LoadGreetings {

  final GreetingRepository greetingRepository;

  public LoadGreetingsUsecase(GreetingRepository greetingRepository) {
    this.greetingRepository = greetingRepository;
  }

  @Override
  public Collection<? extends Greeting> execute() {
    return greetingRepository.getGreetings();
  }
}
