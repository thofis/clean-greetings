package com.gitlab.thofis.cleangreetings;

import com.gitlab.thofis.cleangreetings.model.Greeting;
import com.gitlab.thofis.cleangreetings.model.Name;
import com.gitlab.thofis.cleangreetings.model.Salutation;

class MongoGreeting extends Greeting {
  public MongoGreeting() {
    super(Salutation.HELLO, "<DEFAULT>");
  }

  public void setSalutation(Salutation salutation) {
    this.salutation = salutation;
  }

  public void setName(Name name) {
    this.name = name;
  }

  public void setName(String name) {
    this.name = new Name(name);
  }
}
