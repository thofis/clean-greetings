package com.gitlab.thofis.cleangreetings;

import com.gitlab.thofis.cleangreetings.model.Greeting;
import com.gitlab.thofis.cleangreetings.ports.out.GreetingRepository;
import java.util.Collection;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.stereotype.Repository;

@Repository
public class SpringMongoGreetingRepository implements GreetingRepository {

  private static final Logger log = LoggerFactory.getLogger(SpringMongoGreetingRepository.class);
  private static final String GREETINGS_COLLECTION = "greetings";
  private final MongoTemplate mongoTemplate;

  public SpringMongoGreetingRepository(MongoTemplate mongoTemplate) {
    this.mongoTemplate = mongoTemplate;
  }

  @Override

  public Greeting persistGreeting(Greeting greeting) {
    return mongoTemplate.save(greeting, GREETINGS_COLLECTION);
  }

  @Override
  public Collection<MongoGreeting> getGreetings() {
    final List<MongoGreeting> greetings =
        mongoTemplate.findAll(MongoGreeting.class, GREETINGS_COLLECTION);
    if (log.isDebugEnabled()) {
      greetings.forEach(greeting -> log.debug(greeting.toString()));
    }
    return greetings;
  }
}

