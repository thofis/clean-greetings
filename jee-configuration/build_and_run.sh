docker stop wildfly && docker rm wildfly
mvn clean install docker:build
docker run --name wildfly -p 8080:8080 -p 9990:9990 -d cleangreetings/jee-configuration:latest
docker logs wildfly -f
