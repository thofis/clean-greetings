package com.gitlab.thofis.cleangreetings;

import com.gitlab.thofis.cleangreetings.model.Greeting;
import com.gitlab.thofis.cleangreetings.model.Salutation;
import com.gitlab.thofis.cleangreetings.ports.in.CreateGreeting;
import com.gitlab.thofis.cleangreetings.ports.in.LoadGreetings;
import com.gitlab.thofis.cleangreetings.usecases.CreateGreetingUsecase;
import com.gitlab.thofis.cleangreetings.usecases.LoadGreetingsUsecase;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.PostConstruct;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;
import java.io.Serializable;
import java.util.Collection;

@Named
@RequestScoped
public class GreetingBean implements Serializable {

  private static final Logger log = LoggerFactory.getLogger(GreetingBean.class);
  @Inject
  JpaGreetingRepository greetingRepository;
  private CreateGreeting createGreeting;
  private LoadGreetings loadGreetings;
  private String enteredName;
  private Salutation selectedSalutation;

  @PostConstruct
  public void init() {
    createGreeting = new CreateGreetingUsecase(greetingRepository);
    loadGreetings = new LoadGreetingsUsecase(greetingRepository);

    selectedSalutation = Salutation.HELLO;
    enteredName = "World";
  }

  public String createGreeting() {
    Greeting greeting = createGreeting.execute(selectedSalutation, enteredName);
    log.info("create greeting: {}", greeting);
    return "index.xhtml";
  }

  public Collection<? extends Greeting> getGreetings() {
    return loadGreetings.execute();
  }

  public Salutation[] getSalutations() {
    return Salutation.values();
  }

  public String getEnteredName() {
    return enteredName;
  }

  public void setEnteredName(String enteredName) {
    this.enteredName = enteredName;
  }

  public Salutation getSelectedSalutation() {
    return selectedSalutation;
  }

  public void setSelectedSalutation(Salutation selectedSalutation) {
    this.selectedSalutation = selectedSalutation;
  }
}
