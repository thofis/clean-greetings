package com.gitlab.thofis.cleangreetings;

import com.gitlab.thofis.cleangreetings.model.Greeting;
import com.gitlab.thofis.cleangreetings.model.Salutation;
import com.gitlab.thofis.cleangreetings.usecases.CreateGreetingUsecase;
import com.gitlab.thofis.cleangreetings.usecases.LoadGreetingsUsecase;
import java.util.Collection;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/greeting")
public class GreetingController {

  private final LoadGreetingsUsecase loadGreetingsUsecase;
  private final CreateGreetingUsecase createGreetingUsecase;
  Logger log = LoggerFactory.getLogger(GreetingController.class);

  public GreetingController(LoadGreetingsUsecase loadGreetingsUsecase, CreateGreetingUsecase createGreetingUsecase) {
    this.loadGreetingsUsecase = loadGreetingsUsecase;
    this.createGreetingUsecase = createGreetingUsecase;
  }

  @PostMapping("/{salutation}/{name}")
  public ResponseEntity<Void> postGreeting(@PathVariable String salutation, @PathVariable String name) {
    Salutation salutationEnum = salutation.equals("2") ? Salutation.BONJOUR : Salutation.HELLO;
    final Greeting greeting = createGreetingUsecase.execute(salutationEnum, name);
    log.debug("Greeting created and saved: {}", greeting);
    return ResponseEntity.ok()
        .build();
  }

  @GetMapping()
  public ResponseEntity<Collection<? extends Greeting>> getGreetings() {
    final Collection<? extends Greeting> greetings = loadGreetingsUsecase.execute();
    if (log.isDebugEnabled()) {
      greetings.forEach(greeting -> log.debug("Greeting: {}", greeting));
    }
    return ResponseEntity.ok(greetings);
  }
}
