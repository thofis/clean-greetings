package com.gitlab.thofis.cleangreetings;

import com.gitlab.thofis.cleangreetings.model.Greeting;
import com.gitlab.thofis.cleangreetings.ports.out.GreetingRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.enterprise.context.RequestScoped;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.transaction.Transactional;
import java.util.Collection;
import java.util.List;

import static java.util.stream.Collectors.toList;

@RequestScoped
public class JpaGreetingRepository implements GreetingRepository {

  private static final Logger log = LoggerFactory.getLogger(JpaGreetingRepository.class);

  @PersistenceContext(unitName = "greetings")
  private EntityManager entityManager;

  @Transactional
  @Override
  public Greeting persistGreeting(Greeting greeting) {
    entityManager.persist(new JpaGreeting(greeting));
    entityManager.flush();
    return greeting;
  }

  @Override
  public Collection<? extends Greeting> getGreetings() {
    Query query = entityManager.createQuery("select g from JpaGreeting g", JpaGreeting.class);
    List<JpaGreeting> jpaGreetings = query.getResultList();
    log.warn("" + jpaGreetings.size());
    return jpaGreetings.stream()
                       .map(JpaGreeting::toGreeting)
                       .collect(toList());
  }
}
