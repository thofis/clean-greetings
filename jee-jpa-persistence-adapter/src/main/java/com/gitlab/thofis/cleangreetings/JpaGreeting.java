package com.gitlab.thofis.cleangreetings;

import com.gitlab.thofis.cleangreetings.model.Greeting;
import com.gitlab.thofis.cleangreetings.model.Salutation;

import javax.persistence.*;

@Entity
public class JpaGreeting {

  @Id
  @GeneratedValue
  private long id;

  private String name;

  @Enumerated(EnumType.STRING)
  private Salutation salutation;

  JpaGreeting() {

  }

  JpaGreeting(Greeting greeting) {
    this.name = greeting.getName()
                        .getValue();
    this.salutation = greeting.getSalutation();
  }

  static Greeting toGreeting(JpaGreeting jpaGreeting) {
    return new Greeting(jpaGreeting.salutation, jpaGreeting.name);
  }

}
